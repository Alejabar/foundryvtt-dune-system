export default class CompositeActorSheet extends ActorSheet {
  /** @override */
  _getSubmitData(updateData) {
    const data = super._getSubmitData(updateData);
    const items = {};
    for (let k in data) {
      let r = k.match(/^items\[([^\]]+)\]\.(.*)$/);
      if (r) {
        if (!items[r[1]])
          items[r[1]] = {}
        items[r[1]][r[2]] = data[k];
        delete data[k];
      }
    }
    const up = [];
    for (let k in items) {
      up.push(Object.assign({ _id: k }, items[k]));
    }
    this.actor.updateEmbeddedDocuments("Item", up);
    return data;
  }
}
