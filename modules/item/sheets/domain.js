// -*- js -*-
export default class ItemSheetDuneDomain extends ItemSheet {

  /** @override */
  get template() {
    return `systems/dune/templates/items/domain-sheet.html`;
  }

}
